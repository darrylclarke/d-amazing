# D-Amazing - The Amazon ASIN Reader
(Everything amazing from d to g.)

## Introduction and Instructions
This app takes an Amazon ASIN number and reads details from the URL:
```
www.amazon.com/dp/XXXXXXXX
```
Where XXXXXXXX is the ASIN number.  You can find the ASIN number in the Product Details.  You can also recognize it in the URL of any Amazon page:

> https://www.amazon.com/dp/**B01MTWZJ7A**/ref=sspa_dk_detail_2?psc=1&pd_rd_i=B01MTWZJ7A&pd_rd_wg=a75s4&pd_rd_r=RHG26C6EW2HWYAVXA78Z&pd_rd_w=iTBAF

Paste the ASIN into the "Enter a valid ASIN" box and press "Create".  The details will be downloaded, saved in the local PostreSQL database, and presented in the list in reverse chronological order.

You can delete a product by clicking on the grey 'X' in the top right corner of the detail pane.

## Scraping the Data
I decided to use the Capybara gem to do the scraping.  It uses Webkit and a DSL to load the pages, parse in a jQuery type way, and extract text.  This is all done on the Rails server, and is driven by the API Products controller.

You will note that the code no longer reads the ranks properly.  I had it working, and then Amazon switched the method by which they display the ranks.  I innocently coded another strategy, which worked for a while.  After that strategy stopped working and Amazon was onto a third, I gave up.  I hope the reader can realize that indeed, the ranks could be scraped if they were presented in a consistent way.

I decided to do the scraping on the server for several reasons.  I was familiar with Capybara, making it a logical choice.  It also separates concerns a bit.  The JavaScript front end is only concerned with displaying data, and the Rails back end with procuring and storing data.  To have used a JavaScript library to download the web page and parse it would have been possible, but then the front end would have been doing too much.

### Configuration
The Capybara gem is configured in `config/initializers/amazon_capybara_config.rb`.  This happens once when Rails starts up.

## Other Libraries
Here is a brief description of the libraries I used and why:
### Ruby Gems
#### foreman
I used foreman as a process runner so that the Webpack server and Rails server could be started from the same command.  Simply run:

```
./bin/server
```

from the command line to start both.

#### pg
This is the gem for PostgeSQL database support.

#### active_model_serializers
Using serializers is a neat way to package all the data for a Product together for transport to the JavaScript front end.  It automatically integrates with ActiveRecord so that when you `render json: xxx`, the serializers are automatically invoked.

There are files that control what data is included.  See:  `app/serializers/product_serializer.rb`:

```
class ProductSerializer < ActiveModel::Serializer
  attributes :id,
             :asin,
             :description

  has_many :ranks
  has_many :reviews
end
```

So, for Products, the id, asin, and description are included, as well as the JSON array output from the ranks and reviews serializer.  In effect, you take a Product, and package the whole thing up.

#### interactive_editor, awesome_print, hirb
You may remember these from CodeCore.  They help with debugging in the Rails console.

### JavaScript Libraries
#### @fortawesome/fontawesome and react-fontawesome
I used Font Awesome for the delete button.  I was going to use it for the loading overlay, too, but opted for a more pre-built solution that is described below.

#### superagent
This library is really sugar, but a sweetness that I enjoy using.  One could just as easily use jQuery to do the REST calls, but I am not using jQuery here.  Rather than implement vanilla JS, I leverage this library to quickly do API calls.  I like its simple paradigm and no-nonsense usage.

#### react-loading-overlay
This simple component displays a `<div/>` over the top of the page with a loading spinner and text message.  Rather than implement this on my own, I opted for a pre-built solution to save time.

## Features I am Proud Of
I particularly like the little code generator I wrote to generate the React components.  You will see they are all laid out hierarchically, and in the same general pattern.  This makes it much easier to read and follow what is happening.  I've used this code generator for several personal projects.

A few of the React components, like the `SimpleButton` and `InputBox` were borrowed from another project I was working on.  (They're my own).  A quick read of them shows more complexity than I had time to work into this app.  They are able to communicate via a common parent to enable/disable the button when there is nothing in the edit box.

I also liked using the Active Model Serializers.  I think they provide a higher-order solution that takes *all* of the complexity out of sending data from the back-end to the front-end.  The serializer files themselves are simple and declarative.  One quick read and everything is very clear.

I also like how I read in the seeds from a YML file.  I like the simplicity of YML.  The same component is used to build the ActiveRecord models (`app/service/product_from_hash_factory.rb`) as is used in the `#create` route.

## Stuff I Left Out
There are a number of features and concepts that this Web app doesn't use or leverage.  I'll describe them here:

### Authenticity Token
A traditional Web site will pass a secret token to the front end, which is then passed back in a POST request. This is done with a hidden form field.  This prevents unauthorized or improper use of the server by malicious external agents.

With a pure API server, the necessity for this token is diminished as multiple clients from all over the place may be needing or wanting to communicate.  This Web app is kind of a hybrid.  The Rails server is the only server serving the Web pages that communicate with the server.  So, an authenticity token would be nice, but I didn't have the time to implement it properly.  I just disabled the feature.  (See line 2 of the following snippet)

```
class Api::V1::ProductsController < ApplicationController
  skip_before_action :verify_authenticity_token # ** HERE **

  def index
    all = Product.all.order('created_at desc')
    render json: all, status: :ok
  end
  ...
```

We use authenticity tokens at ModernAdvisor, and I am well versed in harvesting them in our React Native app and feeding them back at the proper time.

### Cross-Origin Resource Sharing - CORS
This is another 'feature' of traditional Web servers that prevents access from any JavaScript front end (by XHR or AJAX calls) other than the one served by the server itself.  The client will do an Options request, then if that succeeds, will make the actual API call.

As mentioned above, this Web app _is_ served by same server as the back end, so CORS isn't an issue that I had to overcome.  It does, however, prevent other agents, such as a mobile app, or pure API client, to connect and read data.

### Factory Girl
The de-facto Rails test implementation would have been rSpec in concert with FactoryGirl to create test objects.  With only three models and limited time, I opted to not implement Factory Girl.  At ModernAdvisor we have over ten times as many objects and Factory Girl is invaluable in providing test objects with different 'traits', or settings.

### Redux / State Management
It would have been nice to implement a proper Redux store to manage the state for my app, but in truth, the app isn't yet feeling the pain that would make it needed yet.  The state is relatively simple, and can easily be maintained in the App component.  I am not passing too much state around.

To implement this well, I would use the Thunk middleware to assist in reading the data from the API.  One makes a call to a `prop` function in `componentDidMount()`, which triggers the API call.  The middleware then makes another Redux call to insert the data into the store.  I have done this in production apps on React Native.

### Re-ordering the list
I simply displayed the list of products in reverse-chronological order.  This design decision was made to limit scope and save time.

### Testing `app/service/product_from_hash_factory.rb`
This component would benefit from some rSpec tests, but I didn't have time to implement that.  Perhaps in a future version.

### Favicon
A favicon would have been nice, but the app works just fine with the stock React icon...

# Final Words
I hope you enjoy reading and running this app as much I enjoyed making it.  I really enjoyed doing the Web scraping, and reverse engineering Amazon's site.  I also enjoyed making the components that comprise the front-end.  It reminds me of playing with Lego when I was a kid.

Enjoy,  
Darryl Clarke  
April 10, 2018  

