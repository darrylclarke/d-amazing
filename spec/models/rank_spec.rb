require 'rails_helper'

RSpec.describe Rank, type: :model do
  def valid_attributes(new_attributes = {})
    {
      ordinal: 133,
      category: 'Computers > Gifts > Stuff',
      product_id: product.id
    }.merge(new_attributes)
  end

  def product
    Product.create({asin:'asin1', description:'desc1'})
  end

  describe 'Validations' do
    it 'creates normally' do
      rank = Rank.new valid_attributes
      expect(rank).to be_valid
    end

    it 'fails with no ordinal' do
      rank = Rank.new valid_attributes({ordinal:nil})
      expect(rank).to be_invalid
    end

    it 'fails with no category' do
      rank = Rank.create valid_attributes({category: nil})
      expect(rank).to be_invalid
    end

    it 'fails with no product' do
      rank = Rank.create valid_attributes({product_id: nil})
      expect(rank).to be_invalid
    end
  end
end

