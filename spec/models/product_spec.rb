require 'rails_helper'

RSpec.describe Product, type: :model do
  def valid_attributes(new_attributes = {})
    {
      asin: 'kdf;alsfdks',
      description: 'Whale bones'
    }.merge(new_attributes)
  end

  describe 'Validations' do
    it 'creates normally' do
      product = Product.new valid_attributes
      expect(product).to be_valid
    end

    it 'fails with no asin' do
      product = Product.new valid_attributes({asin:nil})
      expect(product).to be_invalid
    end

    it 'fails with duplicate asin' do
      product1 = Product.create valid_attributes({asin:'abc'})
      product2 = Product.new valid_attributes({asin:'abc', description: 'another'})
      expect(product1).to be_valid
      expect(product2).to be_invalid
    end

    it 'succeeds when two descriptions are the same' do
      product1 = Product.create valid_attributes({asin:'abc', description: 'same'})
      product2 = Product.new valid_attributes({asin:'def', description: 'same'})
      expect(product1).to be_valid
      expect(product2).to be_valid
    end

  end
end

