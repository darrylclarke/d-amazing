require 'rails_helper'

RSpec.describe Api::V1::ProductsController, type: :controller do
  def create_products
    product1 = Product.create({ asin: 'asin1', description: 'desc1' })
    product1.ranks.create({ ordinal: 1, category: 'abc' })
    product1.reviews.create({
                              reviewer: 'bob',
                              title: 'wow',
                              date_reviewed: Date.today,
                              full_text: 'abcdefg'
                            })
    Product.create({ asin: 'asin2', description: 'desc2' })

  end

  describe 'GET #index' do
    context 'api access' do
      it 'gets all the products' do
        get :index
        expect(response).to have_http_status(:success)
      end
    end
  end



  describe 'POST #create' do
    def valid_response
      {
        error: nil,
        messages: []
      }
    end

    def invalid_response
      {
        error: true,
        messages: ['a', 'b']
      }
    end

    def valid_product
      true
    end

    def invalid_product
      false
    end

    context 'with valid response' do
      before do
        web_scraper_service = double('WebScraperService')
        allow(WebScraperService).to receive(:new).and_return(web_scraper_service)
        allow(web_scraper_service).to receive(:call).and_return(valid_response)
      end

      it 'succeeds' do
        post :create
        # (:create, {:asin => 'asdf1234'})
        expect(response).to have_http_status(:success)
      end
    end

    context 'with valid response' do
      before do
        web_scraper_service = double('WebScraperService')
        allow(WebScraperService).to receive(:new).and_return(web_scraper_service)
        allow(web_scraper_service).to receive(:call).and_return(valid_response)

        product_from_hash_factory = double('ProductFromHashFactory')
        allow(ProductFromHashFactory).to receive(:new).and_return(product_from_hash_factory)
        allow(product_from_hash_factory).to receive(:call).and_return(valid_product)
      end

      it 'succeeds' do
        post :create
        # (:create, {:asin => 'asdf1234'})
        expect(response).to have_http_status(:success)
      end
    end

    context 'with invalid response' do
      before do
        web_scraper_service = double('WebScraperService')
        allow(WebScraperService).to receive(:new).and_return(web_scraper_service)
        allow(web_scraper_service).to receive(:call).and_return(invalid_response)
      end

      it 'fails with error' do
        post :create
        expect(response).to have_http_status(:error)
      end
    end

    context 'with invalid response' do
      before do
        web_scraper_service = double('WebScraperService')
        allow(WebScraperService).to receive(:new).and_return(web_scraper_service)
        allow(web_scraper_service).to receive(:call).and_return(invalid_response)

        product_from_hash_factory = double('ProductFromHashFactory')
        allow(ProductFromHashFactory).to receive(:new).and_return(product_from_hash_factory)
        allow(product_from_hash_factory).to receive(:call).and_return(invalid_product)
      end

      it 'fails with error' do
        post :create
        expect(response).to have_http_status(:error)
      end
    end
  end
end
