require 'capybara'
require 'capybara/dsl'
require 'capybara-webkit'

Capybara.app_host = 'https://www.amazon.com'
Capybara.run_server = false
Capybara.current_driver = :webkit
Capybara::Webkit.configure do |config|
  config.allow_url("www.amazon.com")
  config.block_url("images-na.ssl-images-amazon.com")
  config.block_url("fls-na.amazon.com")
  config.block_url("m.media-amazon.com")
  config.block_url("s.amazon-adsystem.com")
end

