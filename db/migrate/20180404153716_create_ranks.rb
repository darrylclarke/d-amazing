class CreateRanks < ActiveRecord::Migration[5.1]
  def change
    create_table :ranks do |t|
      t.references :product, foreign_key: true
      t.integer :ordinal
      t.string :category

      t.timestamps
    end
  end
end
