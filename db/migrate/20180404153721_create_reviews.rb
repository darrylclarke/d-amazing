class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.references :product, foreign_key: true
      t.string :reviewer
      t.string :title
      t.date :date_reviewed
      t.text :full_text

      t.timestamps
    end
  end
end
