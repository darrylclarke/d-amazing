require 'yaml'
require 'byebug'

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

DATA = YAML::load(File.open('./db/seeds.yml'))

DATA.each do |data|
  ProductFromHashFactory.new(data).call
  # product = Product.find_or_create_by(asin:data['asin'], description: data['description'])
  # data['ranks'].each do |rank|
  #   product.ranks.find_or_create_by(
  #     ordinal: rank['ordinal'],
  #     category: rank['category']
  #   )
  # end
  # data['reviews'].each do |review|
  #   record = product.reviews.find_or_create_by(
  #     date_reviewed: review['date_reviewed'],
  #     reviewer: review['reviewer'],
  #     title: review['title']
  #   )
  #   record.update(full_text: review['full_text'])
  # end
end
