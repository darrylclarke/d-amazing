class ProductFromHashFactory
  def initialize(data)
    @data = data
  end

  def call
    product = Product.find_or_create_by(asin: member(@data, :asin), description: member(@data, :description))
    if member(@data, :ranks)
      member(@data, :ranks).each do |rank|
        product.ranks.find_or_create_by(
          ordinal: member(rank, :ordinal),
          category: member(rank, :category)
        )
      end
    end
    if member(@data, :reviews)
      member(@data, :reviews).each do |review|
        record = product.reviews.find_or_create_by(
          date_reviewed: member(review, :date_reviewed),
          reviewer: member(review, :reviewer),
          title: member(review, :title)
        )
        record.update(full_text: member(review, :full_text))
      end
    end
    product
  end

  private

  def member(source, name)
    source[name] || source[name.to_s]
  end
end
