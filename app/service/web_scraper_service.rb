class WebScraperService
  include Capybara::DSL

  def initialize(asin)
    @url_suffix = "/dp/#{asin}"
    @asin = asin
  end

  def call
    begin
      visit(@url_suffix)
    rescue => exception
      messages = [exception.to_s]
      reason = interpret_error_message(exception.to_s)
      messages.unshift reason if reason
      return { error: true, messages: messages }
    end

    #
    # Get product title, return if it doesn't exist.  The 404 page
    # has no product title.
    #
    data = all('span#productTitle')
    if data.length < 1
      return {error: true, messages: 'Can\'t read that page.  Perhaps an incorrect ASIN?' }
    elsif data.length > 1
      return {error: true, messages: 'I don\'t understand that page' }
    end
    product_title = data[0].text

    #
    # Get reviews
    #
    reviews = []
    all('.reviews-content .review-views .review').each do |review|
      data = review.find('.a-profile-name')
      reviewer = data.text

      data = review.find('span.review-date')
      date = as_date(data.text)

      data = review.find('a.review-title')
      title = data.text

      data = review.find('span.review-text')
      review_text = review_text(data.text)

      # Save the data
      reviews << {
        reviewer: reviewer,
        date_reviewed: date,
        title: title,
        full_text: review_text,
      }
    end

    #
    # Get ranks.  First rank is formatted differently from subsequent ones.
    #
    ranks = []
    all('tr#SalesRank td').each do |data| ##### ==> Get first rank
      processed = first_rank(data.text)
      ranks << as_rank(processed) if processed
    end
    number_text = nil
    all('tr#SalesRank td ul li span').each do |data| ##### ==> Get subsequent ranks
      if data.text =~ /^\#/
        number_text = data.text.gsub(/,/,'') # May have commas!
        next
      end
      processed = subsequent_rank(number_text, data.text)
      ranks << as_rank(processed) if processed
    end
    if ranks.empty?
      data = all('#productDetails_db_sections table.prodDetTable tbody tr')
      ranks = ranks_strategy_two(data)
    end


    #
    # Return a summary of the data
    #
    {
      error: false,
      asin: @asin,
      description: product_title,
      ranks: ranks,
      reviews: reviews,
    }
  end

  private

  def ranks_strategy_two(data)
    ranks = []
    heading_cell_index = data.find_index { |cell| cell.text =~ /Rank/ }
    return [] unless heading_cell_index && heading_cell_index > 0
    data[heading_cell_index].all('td span span').each_with_index do |item, index|
      processed = first_rank(item.text) if index == 0
      processed = strategy_two_get_rank(item.text) if index > 0
      ranks << as_rank(processed) if processed
    end
    return ranks
  end

  def as_rank(array_input)
    {
      ordinal: array_input[0],
      category: array_input[1]
    }
  end

  def first_rank(text)
    matches = text.match /\#([\d|,]*) (in .*)\(/
    return nil unless matches && matches[1] && matches[2]
    [matches[1].gsub(/,/, ''), matches[2]]
  end

  def strategy_two_get_rank(text)
    matches = text.match /\#([\d|,]*) (in .*)/
    return nil unless matches && matches[1] && matches[2]
    [matches[1].gsub(/,/, ''), matches[2]]
  end

  def subsequent_rank(number_text, text)
    if number_text && number_text.length > 0 && number_text[0] == '#'
      [number_text[1..-1], text]
    else
      nil
    end
  end

  def review_text(text)
    matches = text.match /(.*)Read more$/
    return matches[1] if matches && matches[1]
    text
  end

  def as_date(text)
    Date.parse(text)
  end

  def interpret_error_message(message)
    if message =~ /because of error loading https:\/\/www\.amazon\.com/
      'It appears you don\t have a network connection'
    end
  end

end
