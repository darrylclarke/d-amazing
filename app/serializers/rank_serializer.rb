class RankSerializer < ActiveModel::Serializer
  attributes :id,
             :ordinal,
             :category,
             :product_id
end
