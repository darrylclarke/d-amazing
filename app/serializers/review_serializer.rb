class ReviewSerializer < ActiveModel::Serializer
  attributes :id,
             :reviewer,
             :title,
             :date_reviewed,
             :full_text,
             :product_id
end
