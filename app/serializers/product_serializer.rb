class ProductSerializer < ActiveModel::Serializer
  attributes :id,
             :asin,
             :description

  has_many :ranks
  has_many :reviews
end
