class Rank < ApplicationRecord
  belongs_to :product

  validates :ordinal, presence: true
  validates :category, presence: true
end
