class Product < ApplicationRecord
  has_many :reviews, dependent: :destroy
  has_many :ranks, dependent: :destroy

  validates :asin, presence: true, uniqueness: true
  validates :description, presence: true
end
