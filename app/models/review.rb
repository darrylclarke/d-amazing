class Review < ApplicationRecord
  belongs_to :product

  validates :reviewer, presence: true
  validates :title, presence: true
  validates :date_reviewed, presence: true
  validate :review_not_in_future

  private

  def review_not_in_future
    if date_reviewed > Date.today
      errors.add(:date_reviewed, 'Review must not be in the future')
    end
  end
end
