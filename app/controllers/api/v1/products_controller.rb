class Api::V1::ProductsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    all = Product.all.order('created_at desc')
    render json: all, status: :ok
  end

  def create
    new_product = nil
    result = WebScraperService.new(create_params[:asin]).call
    error = result[:error]
    new_product = ProductFromHashFactory.new(result).call unless error
    return render json: {msg: result[:messages]}, status: :error if error
    return render json: {msg: 'Problem converting data'}, status: :error unless new_product
    render json: new_product, status: :ok
  end

  def destroy
    product = Product.find_by(id: destroy_params[:id].to_i)
    if product
      product.destroy
      return render json: nil, status: :ok
    end
    render json: {msg: 'Unknown item, can\'t delete'}, status: :error
  end

  private

  def create_params
    params.permit(:asin)
  end

  def destroy_params
    params.permit(:id)
  end
end
