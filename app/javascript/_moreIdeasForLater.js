// https://stackoverflow.com/questions/35181340/rails-cant-verify-csrf-token-authenticity-when-making-a-post-request

import Spinner from '../spinner';

// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

class Hello extends Component {
  constructor() {
    super();

  }

  render() {
    return (
      <div>
        <div>Hello {this.props.name}! {this.props.appointments.a} / {this.props.appointments.b}</div>
        <App products={this.state.data}/>


      </div>
    );
  }
}

Hello
  .defaultProps = {
  name: 'David'
};

Hello
  .propTypes = {
  name: PropTypes.string
};

import ReactDOM from "react-dom";

document
  .addEventListener(
    'DOMContentLoaded'
    , () => {
      const node = document.getElementById('appointments_data')
      const data = JSON.parse(node.getAttribute('data'))
      ReactDOM
        .render(
          <Hello name="React" appointments={data}/>,
          document.getElementById('react')
          // document.body.appendChild(document.createElement('div')),
        )
    }
  );


/*
 * index.html.erb
 *

<%= stylesheet_pack_tag 'counter-action' %>
<%= stylesheet_pack_tag 'react_loader' %>
<%= javascript_pack_tag 'react_loader' %>

<h1>Pages#index</h1>
<p>Find me in app/views/pages/index.html.erb</p>
<div id="react"></div>
<div class="counter-wrapper">
  <h1>Counter</h1>
  <form class="counter">
    <button id="increment">Increment</button>
    <input type="number" name="counter" id="counter" value="0" />
    <button id="decrement">Decrement</button>
  </form>
</div>
<%= content_tag :div,
                id: "appointments_data",
                data: @appointments.to_json do %>
<% end %>

<%= javascript_pack_tag 'counter-action' %>
<%= javascript_pack_tag 'application' %>

 */
