import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loading-overlay';
import request from 'superagent';
import ProductCard from './components/product-card';
import ErrorCard from './components/error-card';
import ProductListItem from './components/product-list-item';
import InputBox from './components/input-box';
import SimpleButton from './components/simple-button';

const propTypes = {};

// 55B3XO7YRO
// B0794R5H9G
// B079DQZLFW
// B0733B1FF5

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      item: null,
      isLoading: false,
      isError: false,
      errorMessage: '',
      selectedId: null,
    };
    this.asin = null;
    this.create = this.create.bind(this);
    this.loadAllProducts = this.loadAllProducts.bind(this);
    this.selectProduct = this.selectProduct.bind(this);
    this.delete = this.delete.bind(this);
  }

  loadAllProducts() {
    this.setState({
      isLoading: true,
      isError: false,
    });
    request
      .get('/api/v1/products')
      .end((err, res) => {
        const products = JSON.parse(res.text);
        const selectedId = products.length > 0 ? products[0].id : null;
        this.setState({
          products: products,
          isLoading: false,
          selectedId: selectedId,
        })
      });
  }

  create() {
    const asin = this.asin.getData();
    if (!asin || asin.length === 0) {
      return;
    }
    this.setState({
      isLoading: true,
      isError: false,
    });
    request
      .post('/api/v1/products')
      .send({asin: asin})
      .end((err, res) => {
        if (res && !err && res.statusCode === 200) {
          const product = JSON.parse(res.text);
          this.setState({
            products: [product, ...this.state.products],
            isLoading: false,
            selectedId: product.id,
          });
          return;
        }

        // Error case
        const info = JSON.parse(res.text);
        this.setState({
          isLoading: false,
          isError: true,
          errorMessage: info.msg,
        });
      });
  }

  delete(id) {
    this.setState({
      isLoading: true,
      isError: false,
    });
    request
      .delete(`/api/v1/products/${id}`)
      .end((err, res) => {
        if (res && !err && res.statusCode === 200) {
          const selectedIndex = this.state.products.findIndex(product => product.id === id );
          let products = this.state.products;

          if( selectedIndex !== -1) {
            let selectedId = null;
            if (products.length === 1) {
              // selectedId = null is already set
            } else if (selectedIndex === 0) {
              selectedId = products[1].id;
            } else if (selectedIndex === products.length - 1 ) {
              selectedId = products[products.length - 2].id;
            } else {
              selectedId = products[selectedIndex+1].id;
            }
            products.splice(selectedIndex, 1);
            console.log('del', selectedIndex, products.length)
            this.setState({
              products,
              selectedId,
              isLoading: false
            });
          }
          return;
        }

        // Error case
        const info = JSON.parse(res.text);
        this.setState({
          isLoading: false,
          isError: true,
          errorMessage: info.msg,
        })
      });
  }

  componentDidMount() {
    this.loadAllProducts();
  }

  selectProduct(id) {
    this.setState({selectedId: id});
  }

  render() {
    console.log(this.state.products.length);
    const {isLoading} = this.state;
    const selectedProduct = this.state.products.find(product => product.id === this.state.selectedId);
    return (
      <div className={'app'}>
        <div id='header'>
          <div style={{width: '100%', height: '100%'}}>
            <Loadable
              active={isLoading}
              spinner
              text='Really, it worked on my machine...'>
              <div id={'inside-header'}>
                <InputBox ref={(ctrl) => this.asin = ctrl} placeholder={'Enter a valid ASIN'}
                          setStateChange={(ignore) => ignore}/>
                <SimpleButton action={this.create}>Create</SimpleButton>
              </div>
            </Loadable>
          </div>
        </div>
        <div id='info'>

          <div id='left'>
            {this.state.products.map(product => (
              <ProductListItem key={product.id}
                               product={product}
                               selectedId={this.state.selectedId}
                               select={this.selectProduct}/>
            ))}
          </div>
          <div id='details'>
            <ErrorCard display={this.state.isError} msg={this.state.errorMessage}/>
            {selectedProduct && <ProductCard delete={this.delete} product={selectedProduct}/>}
          </div>
        </div>
        <div id='footer'>d-Amazing - Everything amazing from d to g.
        </div>
      </div>
    )
      ;
  }
}

App.propTypes = propTypes;

export default App;
