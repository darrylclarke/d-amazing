import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const propTypes = {
  display: PropTypes.bool.isRequired,
  msg: PropTypes.any.isRequired, // I was trying to support a string or an array
};

class ErrorCard extends Component {
  constructor(props) {
   super(props);
  }

  render() {
    if (!this.props.display) {
      return null;
    }
    const messages = (typeof this.props.msg === 'string') ? [this.props.msg] : this.props.msg;
    return(
      <div className={'error-card'}>
        {messages.map((message,index) => <p key={index}>{message}</p>)}
      </div>
    );
  }
}

ErrorCard.propTypes = propTypes;

export default ErrorCard;
