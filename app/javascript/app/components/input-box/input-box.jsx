import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  input: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  setStateChange: PropTypes.func.isRequired,
};

class InputBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: props.input || ''
    };
    this.input = null;
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ input: event.target.value });
    this.props.setStateChange();
  }

  getData() {
    const text = this.state.input;
    this.setState({input:''});
    this.input.value = '';
    return text;
  }

  done() {
    return this.state.input.length > 0;
  }

  render() {
    const color = this.state.input.length > 0 ? {color: 'black', opacity: 1.0} : null;
    return (
      <div className={'input-box'}>
        <textarea
          ref={(ctrl) => this.input=ctrl}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          style={color}
        />
      </div>
    );
  }
}

InputBox.propTypes = propTypes;

export default InputBox;
