import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  ranks: PropTypes.array.isRequired,
};

class RanksCard extends Component {
  constructor(props) {
   super(props);
  }

  render() {
    return (
      <div>
        <p className={'product-card__title'}>Ranks</p>
        {this.props.ranks.map(rank => (
          <p key={rank.id} className={'product-card__indented-item'}>{rank.ordinal} - {rank.category}</p>
        ))}
      </div>
    );
  }
}

RanksCard.propTypes = propTypes;

export default RanksCard;
