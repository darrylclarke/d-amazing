import React, {Component} from 'react';
import PropTypes from 'prop-types';
//import styled from 'styled-components';

const propTypes = {
  reviews: PropTypes.array.isRequired,
};

class ReviewsCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <p className={'product-card__title'}>Reviews</p>
        {this.props.reviews.map(review => (
          <div key={review.id} >
            <p className={'product-card__indented-item'}>{review.date_reviewed} - {review.reviewer} - {review.title}</p>
            <p className={'product-card__review-text'}>{review.full_text}</p>
          </div>
        ))}
      </div>
    );
  }
}

ReviewsCard.propTypes = propTypes;

export default ReviewsCard;
