import React, {Component} from 'react';
import PropTypes from 'prop-types';
import RanksCard from './components/ranks-card';
import ReviewsCard from './components/reviews-card';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faTimes from '@fortawesome/fontawesome-free-solid/faTimes';

const propTypes = {
  product: PropTypes.object.isRequired,
  delete: PropTypes.func.isRequired,
};

class ProductCard extends Component {
  constructor(props) {
    super(props);
  }

  get asin() {
    return this.props.product && this.props.product.asin;
  }

  get description() {
    return this.props.product && this.props.product.description;
  }

  get ranks() {
    return this.props.product && this.props.product.ranks ? this.props.product.ranks : [];
  }

  get reviews() {
    return this.props.product && this.props.product.reviews ? this.props.product.reviews : [];
  }

  render() {
    return (
      <div className={'product-card'}>

        <div className={'product-card__delete-button'}>
          <a className={''} onClick={() => this.props.delete(this.props.product.id)}>
            <FontAwesomeIcon icon={faTimes} color={'grey'}/>
          </a>
        </div>

        <p><span className={'product-card__title'}>ASIN: </span>{this.asin}</p>
        <p><span className={'product-card__title'}>Description: </span>{this.description}</p>
        <div>
          <RanksCard ranks={this.ranks || []}/>
          <ReviewsCard reviews={this.reviews || []}/>
        </div>
      </div>
    );
  }
}

ProductCard.propTypes = propTypes;

export default ProductCard;
