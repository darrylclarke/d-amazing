import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  product: PropTypes.object.isRequired,
  selectedId: PropTypes.number.isRequired,
  select: PropTypes.func.isRequired,
};

class ProductListItem extends Component {
  constructor(props) {
    super(props);
    this.doSelect = this.doSelect.bind(this);
  }

  get description() {
    return this.props.product && this.props.product.description;
  }

  doSelect() {
    this.props.select(this.props.product.id);
  }

  render() {
    const isSelected = (this.props.selectedId === this.props.product.id);
    return (
      <div className={'product-list-item'}>
        <a   className={'product-list-item__button'} onClick={this.doSelect}>
          <p className={isSelected ? 'product-list-item__button-selected' : ''}>{this.description}</p>
        </a>
      </div>
    );
  }
}

ProductListItem.propTypes = propTypes;

export default ProductListItem;
