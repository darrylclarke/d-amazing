import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  action: PropTypes.func.isRequired,
  style: PropTypes.object,
  innerClass: PropTypes.string,
  disabled: PropTypes.bool,
};

class SimpleButton extends Component {
  constructor(props) {
    super(props);
  }

  action() {
    if (!this.props.disabled) {
      this.props.action()
    }
  }

  render() {
    const color = this.props.disabled ? {opacity: 0.5} : {};
    return (
      <div className={'simple-button ' + this.props.innerClass}>
        <button style={{...this.props.style, ...color}}
                onClick={this.action.bind(this)}
        >{this.props.children}</button>
      </div>
    );
  }
}

SimpleButton.propTypes = propTypes;

export default SimpleButton;
