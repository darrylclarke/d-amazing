import React from 'react'
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner';

const Spinner = () => <FontAwesomeIcon icon={faSpinner} size="6x" pulse/>;

export default Spinner;
